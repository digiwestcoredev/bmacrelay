// BluemacDataServer.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <stdio.h>
#include <string.h>

#include <string>

#include "sslconnection.h"

std::string CSslConnection::s_certificateFilename;
std::string CSslConnection::s_privateKeyFilename;

CSslConnection *CSslConnection::Create(
    SOCKET socket
    )
{
    CSslConnection *pSslConnection = new CSslConnection();

    if (pSslConnection == nullptr)
    {
        return nullptr;
    }

    bool brc = pSslConnection->InitializeContext(socket);

    if (!brc)
    {
        delete pSslConnection;
        return nullptr;
    }

    return pSslConnection;
}

CSslConnection::CSslConnection()
{
    m_socket = INVALID_SOCKET;
    m_pCtx = nullptr;
    m_ssl = nullptr;
}

CSslConnection::~CSslConnection()
{
    Shutdown();
}

char *CSslConnection::ossl_err_as_string()
{
    BIO *bio = BIO_new(BIO_s_mem());

    ERR_print_errors(bio);

    char *buf = NULL;
    size_t len = BIO_get_mem_data(bio, &buf);

    char *ret = (char *) calloc(1, 1 + len);

    if (ret)
    {
        memcpy(ret, buf, len);
    }

    BIO_free(bio);

    return ret;
}

void CSslConnection::Initialize(
    const std::string &certificateFilename,
    const std::string &privateKeyFilename
    )
{
    s_certificateFilename = certificateFilename;
    s_privateKeyFilename = privateKeyFilename;

    // load & register all cryptos, etc.
    OpenSSL_add_all_algorithms();

#ifdef DEBUG
    // load all error messages
    SSL_load_error_strings();
#endif

    // create new server-method instance
    SSLv23_server_method();
    // TLSv1_2_server_method();
}

void CSslConnection::CleanUp()
{
#ifdef DEBUG
    ERR_free_strings();
#endif

    ERR_clear_error();
    EVP_cleanup();
}

bool CSslConnection::InitializeContext(
    SOCKET socket
    )
{
    m_socket = socket;

    // create new context for client method
    m_pCtx = SSL_CTX_new(SSLv23_server_method());

    if (m_pCtx == nullptr)
    {
#ifdef DEBUG
        ERR_print_errors_fp(stderr);
#endif

        return false;
    }

    SSL_CTX_set_session_id_context(
            m_pCtx,
            (const unsigned char *) "relayserver",
            (unsigned int) sizeof("relayserver") - 1
            );

    SSL_CTX_set_session_cache_mode(m_pCtx, SSL_SESS_CACHE_OFF);
    SSL_CTX_set_mode(m_pCtx, SSL_MODE_AUTO_RETRY);

    // get new SSL state with context
    m_ssl = SSL_new(m_pCtx);

    if (m_ssl == nullptr)
    {
        return false;
    }

    SSL_set_mode(m_ssl, SSL_MODE_AUTO_RETRY);

    printf("Loading certificates\n");

    // Load certificates
    bool brc = LoadSSLCertificates();

    if (!brc)
    {
        return false;
    }

    // set connection socket to SSL state
    SSL_set_fd(m_ssl, (int) m_socket);

    ERR_clear_error();

    int rc = SSL_accept(m_ssl);

    if (rc != 1)
    {
        printf("SSL_accept failed with rc=%d\n", SSL_get_error(m_ssl, rc));
        return false;
    }

    ERR_clear_error();

    return true;
}

bool CSslConnection::LoadSSLCertificates()
{
    /* set the local certificate from CertFile */
    if (SSL_use_certificate_file(m_ssl, s_certificateFilename.c_str(), SSL_FILETYPE_PEM) != 1)
    {
        char *sslErrorString = ossl_err_as_string();

        printf("SSL_use_certificate_file(%s) failed with error '%s'\n", s_certificateFilename.c_str(), (sslErrorString != nullptr) ? sslErrorString : "UNKNOWN ERROR");

        if (sslErrorString != nullptr)
        {
            free(sslErrorString);
        }

        return false;
    }

    /* set the private key from KeyFile (may be the same as CertFile) */
    if (SSL_use_PrivateKey_file(m_ssl, s_privateKeyFilename.c_str(), SSL_FILETYPE_PEM) != 1)
    {
        char *sslErrorString = ossl_err_as_string();

        printf("SSL_use_PrivateKey_file(%s) failed with error '%s'\n", s_privateKeyFilename.c_str(), (sslErrorString != nullptr) ? sslErrorString : "UNKNOWN ERROR");

        if (sslErrorString != nullptr)
        {
            free(sslErrorString);
        }

        return false;
    }

    /* verify private key */
    if (!SSL_check_private_key(m_ssl))
    {
        printf("Private key does not match the public certificate\n");

        return false;
    }

    return true;
}

void CSslConnection::Shutdown()
{
    if (m_ssl != nullptr)
    {
        SSL_shutdown(m_ssl);
        SSL_free(m_ssl);
        m_ssl = nullptr;
    }

    if (m_socket != INVALID_SOCKET)
    {
        close(m_socket);
        m_socket = INVALID_SOCKET;
    }

    if (m_pCtx != nullptr)
    {
        SSL_CTX_free(m_pCtx);
        m_pCtx = nullptr;
    }

    return;
}

SSL *CSslConnection::getSsl()
{
    return m_ssl;
}

bool CSslConnection::RecvBytes(
    char *pb,
    uint32_t cb
    )
{
    int rc = 0;
    char *pBufferPosition = pb;
    int receivedBytes;
    int bytesRemaining = (int) cb;

    // Loop until we receive messageLength bytes or an error occurs.
    do
    {
        ERR_clear_error();

        receivedBytes = SSL_read(m_ssl, pBufferPosition, bytesRemaining);

        if (receivedBytes <= 0)
        {
#ifdef _DEBUG
            ERR_print_errors_fp(stderr);
#endif

            rc = SSL_get_error(m_ssl, receivedBytes);

            printf("SSL_read failed with rc=%i\n", rc);
            break;
        }

        bytesRemaining -= receivedBytes;
        pBufferPosition += receivedBytes;
    } while (bytesRemaining > 0);

    return (bytesRemaining == 0);
}

