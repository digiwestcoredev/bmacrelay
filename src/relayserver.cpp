#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <string>

#include <sys/stat.h>

#include "sslconnection.h"
#include "BluemacDecoder.h"

typedef struct _RELAYSERVER_REGISTRATION_MESSAGE_HEADER
{
    uint32_t versionMajor;
    uint32_t versionMinor;
    uint32_t currentTime;
    uint32_t cbPrivateKey;
} RELAYSERVER_REGISTRATION_MESSAGE_HEADER, *PRELAYSERVER_REGISTRATION_MESSAGE_HEADER;

typedef struct _RELAYSERVER_MESSAGE_HEADER
{
    uint32_t currentTime;
    uint32_t cbData;
    char serialNumber[40 + 1];   // 40 is max size for a serial number.  Add 1 for a NULL-terminator.
    char deviceType[50 + 1];     // 50 is max size for a device type.  Add 1 for a NULL-terminator.
} RELAYSERVER_MESSAGE_HEADER, *PRELAYSERVER_MESSAGE_HEADER;

static SOCKET g_listenSocket = INVALID_SOCKET;
static volatile sig_atomic_t g_terminateIncomingConnectionsLoop = 0;
static std::string g_allowedIPAddress;
static std::string g_certificateFilename;
static std::string g_privateKeyFilename;

std::string EpochTimeToGMTDateTimeString(
    time_t epochTime
    )
{
    char buffer[sizeof("YYYY-MM-DD HH:MM:SS")];
    struct tm *ptm;

    ptm = gmtime(&epochTime);
    strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", ptm);

    return buffer;
}

bool sizeMessageBuffer(
    char **ppbRecvBuffer,
    uint32_t *pcbRecvBuffer,
    uint32_t cbRequired
    )
{
    bool brc = true;

    if (cbRequired > *pcbRecvBuffer)
    {
        char *pbRecvBuffer = (char *) realloc(*ppbRecvBuffer, cbRequired);

        if (pbRecvBuffer != nullptr)
        {
            *ppbRecvBuffer = pbRecvBuffer;
            *pcbRecvBuffer = cbRequired;
        }
        else
        {
            brc = false;
        }
    }

    return brc;
}

//
// =======================================================================
//
// getSubjectByNID - get the subject key field value by NID index
//
int getSubjectByNID(
    X509 *cert,
    int nid_name,
    char *field
    )
{
    X509_NAME *subject = X509_get_subject_name(cert);

    if (subject == NULL)
    {
        return 1;
    }

    int position = X509_NAME_get_index_by_NID(subject, nid_name, -1);
    X509_NAME_ENTRY *entry = X509_NAME_get_entry(subject, position);

    if (entry == NULL)
    {
        return 1;
    }

    ASN1_STRING *asn1Data = X509_NAME_ENTRY_get_data(entry);

    if (asn1Data == NULL)
    {
        return 1;
    }

    strcpy(field, (char *)ASN1_STRING_get0_data(asn1Data));
    
    return 0;
}

int validateCertificate(
    const std::string &certificateFilename,
    SSL *ssl
    )
{
    char msg[256];
    int retVal = 1;
    X509 *cert = SSL_get_peer_certificate(ssl);
    EVP_PKEY *certKey = NULL;
    EVP_PKEY *servKey = NULL;
    FILE *fp = NULL;
    BIO *bpCert = BIO_new(BIO_s_mem());
    BIO *bpServ = BIO_new(BIO_s_mem());
    BUF_MEM *pCertMem = NULL;
    BUF_MEM *pServMem = NULL;

    do
    {
        // validate input params
        if (cert == NULL) 
        {
            printf("Certificate validation failed with invalid input params\n");
            break;
        }

        // load public from certificate
        certKey = X509_get_pubkey(cert);

        if (certKey == NULL)
        {
            printf("Certificate validation failed X509_get_pubkey\n");
            break;
        }

        // write public key to bio buffer
        if (PEM_write_bio_PUBKEY(bpCert, certKey) <= 0)
        {
            printf("Certificate validation failed PEM_write_bio_PUBKEY\n");
            break;
        }

        BIO_write(bpCert, "\0", 1); // null terminate
        BIO_get_mem_ptr(bpCert, &pCertMem);

        // load corresponding privatekey for streaming server
        fp = fopen(certificateFilename.c_str(), "r");

        if (fp == NULL)
        {
            printf("Certificate validation failed, unable to open certificate file %s\n", certificateFilename.c_str());
            break;
        }

        servKey = EVP_PKEY_new();
        PEM_read_PrivateKey(fp, &servKey, NULL, NULL);

        if (servKey == NULL) 
        {
            printf("Certificate validation failed PEM_read_PrivateKey\n");
            break;
        }

        // write public key to bio buffer
        if (PEM_write_bio_PUBKEY(bpServ, servKey) <= 0)
        {
            printf("Certificate validation failed PEM_write_bio_PUBKEY\n");
            break;
        }

        BIO_write(bpServ, "\0", 1); // null terminate
        BIO_get_mem_ptr(bpServ, &pServMem);
        
        // verify the public key from cert matches the public key portion of private key for server
        int ret = memcmp(pCertMem->data, pServMem->data, pCertMem->length);

        if (pCertMem->length != pServMem->length || ret != 0)
        {
            printf("Certificate validation failed to match pubkey from certificate %s\n", certificateFilename.c_str());
            break;
        }

        // extract certificate info and log the details
        char unit[128];
        char org[128];
        char url[128];
        char country[10];
        char state[20];
        char city[40];

        getSubjectByNID(cert, NID_organizationalUnitName, unit);
        getSubjectByNID(cert, NID_organizationName, org);
        getSubjectByNID(cert, NID_commonName, url);
        getSubjectByNID(cert, NID_countryName, country);
        getSubjectByNID(cert, NID_stateOrProvinceName, state);
        getSubjectByNID(cert, NID_localityName, city);

        ASN1_TIME *notBefore = NULL;
        int len = 32;
        char nbTime[len];
        struct tm tm_time;

        notBefore = X509_get_notBefore(cert);

        if (notBefore != NULL && notBefore->type == V_ASN1_UTCTIME)
        {
            strptime((const char *)notBefore->data, "%y%m%d%H%M%SZ", &tm_time);
            strftime(nbTime, sizeof(char)*len, "%h %d %H:%M:%S %Y", &tm_time);
        }
        
        // log cert validation result
        sprintf(msg, "%s, %s, %s, %s, %s, %s", nbTime, unit, org, city, state, url);
        printf("validateCert %s\n", msg);
        
        // success
        retVal = 0;
        
    } while (0);
    
    // free dynamically allocated resources
    if (cert != NULL)
    {
        X509_free(cert);
    }

    if (fp != NULL)
    {
        fclose(fp);
    }

    if (certKey != NULL)
    {
        EVP_PKEY_free(certKey);
    }

    if (servKey != NULL)
    {
        EVP_PKEY_free(servKey);
    }

    if (bpCert != NULL)
    {
        BIO_set_close(bpCert, BIO_CLOSE);
        BIO_free(bpCert);
    }

    if (bpServ != NULL)
    {
        BIO_set_close(bpServ, BIO_CLOSE);
        BIO_free(bpServ);
    }

    return retVal;
}

bool receive_registration_message(
    CSslConnection *pSslConnection,
    PRELAYSERVER_REGISTRATION_MESSAGE_HEADER pRegistrationMessageHeader,
    char **ppbMessage,
    uint32_t *pcbMessage
    )
{
    printf("Receiving registration message header...\n");

    // First receive the registration header.
    bool brc = pSslConnection->RecvBytes(
            (char *) pRegistrationMessageHeader,
            sizeof(RELAYSERVER_REGISTRATION_MESSAGE_HEADER)
            );

    uint32_t currentTime;
    uint32_t cbPrivateKey;

    if (brc)
    {
        currentTime = ntohl(pRegistrationMessageHeader->currentTime);
        cbPrivateKey = ntohl(pRegistrationMessageHeader->cbPrivateKey);

        printf("Received registration message header: time=%d, cbPrivateKey=%d\n", currentTime, cbPrivateKey);

        // Ensure we have enough space for the private key.
        brc = sizeMessageBuffer(ppbMessage, pcbMessage, cbPrivateKey);
    }

    if (brc)
    {
        // Now receive the private key.
        brc = pSslConnection->RecvBytes(*ppbMessage, cbPrivateKey);
    }

    if (brc)
    {
        // Verify the private key
        char *pchClientPrivateKey = *ppbMessage;
        struct stat fileStat;
        FILE *privateKeyFile = NULL;
        char *pchPrivateKeyFileContents = nullptr;

        if (stat(g_privateKeyFilename.c_str(), &fileStat) != 0)
        {
            printf("Failed to get size of private key file %s\n", g_privateKeyFilename.c_str());
            brc = false;
        }

        if (cbPrivateKey != fileStat.st_size)
        {
            printf("Client private key length %d does not match the size of the private key file (%li)\n", cbPrivateKey, fileStat.st_size);
            brc = false;
        }

        if (brc)
        {
            privateKeyFile = fopen(g_privateKeyFilename.c_str(), "rb");

            if (privateKeyFile == NULL)
            {
                printf("Failed to open private key file %s - errno=%d\n", g_privateKeyFilename.c_str(), errno);
                brc = false;
            }
        }

        if (brc)
        {
            pchPrivateKeyFileContents = (char *) malloc(fileStat.st_size);

            if (pchPrivateKeyFileContents == nullptr)
            {
                printf("Failed to allocate memory of %li bytes for reading private key file contents\n", fileStat.st_size);
                brc = false;
            }
        }

        if (brc)
        {
            if (fread(pchPrivateKeyFileContents, fileStat.st_size, 1, privateKeyFile) != 1)
            {
                printf("Failed to read contents of private key file\n");
                brc = false;
            }
        }

        if (brc)
        {
            if (memcmp(pchClientPrivateKey, pchPrivateKeyFileContents, cbPrivateKey))
            {
                printf("Private key supplied by client does not match the contents of private key file %s!\n", g_privateKeyFilename.c_str());
                brc = false;
            }

        }

        if (privateKeyFile != NULL)
        {
            fclose(privateKeyFile);
        }

        free(pchPrivateKeyFileContents);
    }

    return brc;
}

bool receive_data_message(
    CSslConnection *pSslConnection,
    PRELAYSERVER_MESSAGE_HEADER pDataMessageHeader,
    char **ppbMessage,
    uint32_t *pcbMessage
    )
{
    int cbMessage;

    // First receive the message header.
    bool brc = pSslConnection->RecvBytes(
            (char *) pDataMessageHeader,
            sizeof(RELAYSERVER_MESSAGE_HEADER)
            );

    if (brc)
    {
        // Ensure we have enough space for the data.
        brc = sizeMessageBuffer(ppbMessage, pcbMessage, pDataMessageHeader->cbData);
    }

    if (brc)
    {
        cbMessage = ntohl(pDataMessageHeader->cbData);

        // Now receive the private key.
        brc = pSslConnection->RecvBytes(*ppbMessage, cbMessage);
    }

    if (brc)
    {
        // One or more samples may have been sent.  Each one is terminated by '\n' character.
        char *pchEndOfData = *ppbMessage + cbMessage;
        char *pchStartSample;
        char *pchEndSample;
        BLUEMACSAMPLE sample;

        for (
            pchStartSample = *ppbMessage, pchEndSample = strchr(pchStartSample, '\n');
            ((pchStartSample < pchEndOfData) && (pchEndSample != nullptr));
            pchStartSample = pchEndSample + 1, pchEndSample = strchr(pchStartSample, '\n')
            )
        {
            *pchEndSample = '\0';

            brc = BluemacDecodeRawSample(pchStartSample, &sample);

            if (brc)
            {
                printf(
                        "Sample from device %s: timestamp=%s, record type=%s, objectId=%s, radioType=%s, rssi=%i, duration=%d, hitcount=%d, oui=%d, manufacturer=%d\n",
                        pDataMessageHeader->serialNumber,
                        EpochTimeToGMTDateTimeString(sample.timestamp).c_str(),
                        BluemacGetRecordType(sample.reductionType).c_str(),
                        sample.objectId,
                        sample.radioType,
                        sample.rssi,
                        sample.reductionDuration,
                        sample.reductionHitCount,
                        sample.oui,
                        sample.manufacturer
                        );
            }
        }
    }

    return brc;
}

void socket_connected(
    SOCKET socket
    )
{
    // Set up SSL
    CSslConnection *pSslConnection = CSslConnection::Create(socket);

    if (pSslConnection == nullptr)
    {
        return;
    }

    // Verify certificate
    bool brc = validateCertificate(g_certificateFilename, pSslConnection->getSsl());

    // This buffer will get reallocated by the recv() methods as necessary to accomodate incoming messages.
    char *pMessageBuffer = nullptr;
    uint32_t cbMessageBuffer = 0;

    RELAYSERVER_REGISTRATION_MESSAGE_HEADER registrationMessageHeader;
    RELAYSERVER_MESSAGE_HEADER dataMessageHeader;

    brc = receive_registration_message(pSslConnection, &registrationMessageHeader, &pMessageBuffer, &cbMessageBuffer);

    while (brc)
    {
        brc = receive_data_message(pSslConnection, &dataMessageHeader, &pMessageBuffer, &cbMessageBuffer);

        if (!brc)
        {
            printf("Error receiving message - exiting.\n");
            break;
        }
    }

    delete pSslConnection;
    free(pMessageBuffer);

    return;
}

bool listen_for_connection(
    int port
    )
{
    struct sockaddr_in serverSockAddr;
    struct sockaddr_in client;
    socklen_t addrlen = sizeof(serverSockAddr);
    SOCKET clientSocket = INVALID_SOCKET;

    // Create listener socket
    g_listenSocket = socket(AF_INET, SOCK_STREAM, 0);

    if (g_listenSocket == INVALID_SOCKET)
    {
        printf("Could not create socket, exiting.\n");
        return 1;
    }

    // Prepare the sockaddr_in structure
    serverSockAddr.sin_family = AF_INET;         // host byte order
    serverSockAddr.sin_addr.s_addr = INADDR_ANY; // accept connections from any IP
    serverSockAddr.sin_port = htons(port);       // set TCP port
    memset(&(serverSockAddr.sin_zero), '\0', 8); // zero out rest of structure

    // Bind
    int reuse = 1;

    if (setsockopt(g_listenSocket, SOL_SOCKET, SO_REUSEADDR, (const char *) &reuse, sizeof(reuse)) < 0)
    {
        printf("setsockopt(SO_REUSEADDR) failed.\n");
    }

#ifdef SO_REUSEPORT
    if (setsockopt(g_listenSocket, SOL_SOCKET, SO_REUSEPORT, (const char *) &reuse, sizeof(reuse)) < 0)
    {
        printf("setsockopt(SO_REUSEPORT) failed\n");
    }
#endif

    if (bind(g_listenSocket, (struct sockaddr *) &serverSockAddr, sizeof(serverSockAddr)) < 0)
    {
        printf("Socket bind to port %d failed, cleaning up and exiting.\n", port);

        close(g_listenSocket);

        return false;
    }

    // Listen (socket, queue length for pending connections)
    printf("Listening on port %d\n", port);

    if (listen(g_listenSocket, 0) < 0)
    {
        printf("Socket Listen on port %d failed, cleaning up and exiting.\n", port);

        close(g_listenSocket);

        return false;
    }

    while (!g_terminateIncomingConnectionsLoop)
    {
        // Accept inncoming device connections
        printf("Waiting for incoming connections...\n");

        clientSocket = accept(g_listenSocket, (struct sockaddr *) &client, &addrlen);

        if (g_terminateIncomingConnectionsLoop)
        {
            break;
        }

        char clientIPAddress[128];

#ifdef _WIN32
        inet_ntop(client.sin_family, &client, clientIPAddress, sizeof(clientIPAddress));

        printf("New connection from IP address %s.\n", clientIPAddress);
#else
        strncpy(clientIPAddress, inet_ntoa(client.sin_addr), sizeof(clientIPAddress));
        clientIPAddress[sizeof(clientIPAddress) - 1] = '\0';

        printf("New connection from IP address %s.\n", clientIPAddress);
#endif

        // If allowedIPAddress is set, then validate the connecting IP address matches it.
        if (!g_allowedIPAddress.empty() && strcmp(g_allowedIPAddress.c_str(), clientIPAddress))
        {
            printf("Connection from IP address %s not accepted (only accept connection from %s)\n", clientIPAddress, g_allowedIPAddress.c_str());
        }
        else
        {
            socket_connected(clientSocket);
        }

        shutdown(clientSocket, SHUT_RDWR);
        close(clientSocket);
    }

    printf("Signal received, cleaning up and exiting.\n");

    // Shutdown listener socket and close it.
    shutdown(g_listenSocket, SHUT_RDWR);
    close(g_listenSocket);

    return true;
}

bool relay_server_start(
    uint32_t port,
    const std::string &allowedIPAddress,
    const std::string &certificateFilename,
    const std::string &privateKeyFilename
    )
{
    g_allowedIPAddress = allowedIPAddress;
    g_certificateFilename = certificateFilename;
    g_privateKeyFilename = privateKeyFilename;

#ifdef _WIN32
    WSADATA wsaData;

    if (WSAStartup(MAKEWORD(2, 2), &wsaData) != NO_ERROR)
    {
        printf("WSAStartup() failed, exiting.\n");
        return false;
    }
#endif

#ifndef _WIN32
    // Ignore SIGPIPE which can get sent if we try to do anything on a socket that is closed or in an error state.
    // The only thing we ever try to do after an error is close it down (which can cause SIGPIPE to be sent).
    signal(SIGPIPE, SIG_IGN);
#endif

    CSslConnection::Initialize(certificateFilename, privateKeyFilename);

    bool brc = listen_for_connection(port);

    CSslConnection::CleanUp();

#ifdef _WIN32
    WSACleanup();
#endif

    return brc;
}

void relay_server_stop()
{
    g_terminateIncomingConnectionsLoop = 1;

    // Close the g_listenSocket socket which will cause blocking call to accept to fail.
    if (g_listenSocket != INVALID_SOCKET)
    {
        close(g_listenSocket);
        g_listenSocket = INVALID_SOCKET;
    }

    return;
}

