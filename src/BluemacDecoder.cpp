#include <string.h>
#include <stdlib.h>

#include "BluemacDecoder.h"

#define min(a, b)    ((a<b) ? a : b)

bool BluemacDecodeRawSample(
    const char *pchSample,
    PBLUEMACSAMPLE pBluemacSample
    )
{
    // There are up to 11 fields that are comma-delimited.  Convert them to BLUEMACSAMPLE data type.

    //     1. timestamp as epoch time
    const char *pchFieldStart = pchSample;
    const char *pchFieldEnd = strchr(pchFieldStart, ',');

    if (pchFieldEnd == nullptr)
    {
        return false;
    }

    pBluemacSample->timestamp = strtoul(pchFieldStart, nullptr, 10);

    // Make sure we have a valid timestamp.
    if (pBluemacSample->timestamp == 0)
    {
        return false;
    }

    //     2. Object ID terminated by , field delimiter
    pchFieldStart = pchFieldEnd + 1;
    pchFieldEnd = strchr(pchFieldStart, ',');

    if (pchFieldEnd == nullptr)
    {
        return false;
    }

    uint32_t fieldLen = (uint32_t) (pchFieldEnd - pchFieldStart);
    strncpy(pBluemacSample->objectId, pchFieldStart, min(fieldLen, (uint32_t) sizeof(pBluemacSample->objectId) - 1));
    pBluemacSample->objectId[min(fieldLen, (uint32_t) sizeof(pBluemacSample->objectId) - 1)] = '\0';

    //     3. RSSI as a number
    pchFieldStart = pchFieldEnd + 1;
    pchFieldEnd = strchr(pchFieldStart, ',');

    if (pchFieldEnd == nullptr)
    {
        return false;
    }

    pBluemacSample->rssi = (int8_t) strtol(pchFieldStart, nullptr, 10);

    //     4. Radio type as bt, btle, or wifi.
    pchFieldStart = pchFieldEnd + 1;
    pchFieldEnd = strchr(pchFieldStart, ',');

    if (pchFieldEnd == nullptr)
    {
        return false;
    }

    fieldLen = pchFieldEnd - pchFieldStart;
    strncpy(pBluemacSample->radioType, pchFieldStart, min(fieldLen, (uint32_t) sizeof(pBluemacSample->radioType) - 1));
    pBluemacSample->radioType[min(fieldLen, (uint32_t) sizeof(pBluemacSample->radioType) - 1)] = '\0';

    //     5. OUI as 6-hex digits.
    pchFieldStart = pchFieldEnd + 1;
    pchFieldEnd = strchr(pchFieldStart, ',');

    pBluemacSample->oui = (uint32_t) strtoul(pchFieldStart, nullptr, 16);

    //     6. Manufacturer as 4-hex digits (BLE) or empty for BT/WiFi.
    pchFieldStart = pchFieldEnd + 1;
    pchFieldEnd = strchr(pchFieldStart, ',');

    pBluemacSample->manufacturer = (uint16_t) strtoul(pchFieldStart, nullptr, 16);

    //     7. One character indicator if this is a MAC-reduced record or not.
    pchFieldStart = pchFieldEnd + 1;

    if (*pchFieldStart == 'O')
    {
        // Reduction is off
        pBluemacSample->reductionType = BLUEMAC_REDUCTIONTYPE_OFF;
    }
    else if (*pchFieldStart == 'N')
    {
        // Reduction is on and this is a New hit
        pBluemacSample->reductionType = BLUEMAC_REDUCTIONTYPE_NEWHIT;

        // These fields should be ignored for new hit, but we'll set them anyway.
        pBluemacSample->reductionHitCount = 1;
        pBluemacSample->reductionDuration = 0;
    }
    else if (*pchFieldStart == 'D')
    {
        // Reduction is on and this is an update to a previous N record.  There are two more fields.
        pBluemacSample->reductionType = BLUEMAC_REDUCTIONTYPE_UPDATEDHIT;

        //     8. hitcount as a number
        pchFieldStart += 2; // Advance past the reduction type and the comma delimiter
        pchFieldEnd = strchr(pchFieldStart, ',');

        pBluemacSample->reductionHitCount = (uint32_t) strtoul(pchFieldStart, nullptr, 10);

        //     9. duration as a number
        pchFieldStart = pchFieldEnd + 1;

        pBluemacSample->reductionDuration = (uint32_t) strtoul(pchFieldStart, nullptr, 10);

        //     10. strongest RSSI timestamp as a number
        pchFieldStart = pchFieldEnd + 1;

        pBluemacSample->strongestRSSITimestamp = (uint32_t) strtoul(pchFieldStart, nullptr, 10);

        //     11. strongest RSSI as a number
        pchFieldStart = pchFieldEnd + 1;

        pBluemacSample->strongestRSSI = (uint32_t) strtoul(pchFieldStart, nullptr, 10);
    }
    else
    {
        pBluemacSample->reductionType = BLUEMAC_REDUCTIONTYPE_UNKNOWN;
        return false;
    }

    return true;
}

std::string BluemacGetRecordType(
    uint32_t recordType
    )
{
    const char *pszRecordType;

    switch (recordType)
    {
        case BLUEMAC_REDUCTIONTYPE_OFF:
            pszRecordType = "raw";
            break;

        case BLUEMAC_REDUCTIONTYPE_NEWHIT:
            pszRecordType = "new";
            break;

        case BLUEMAC_REDUCTIONTYPE_UPDATEDHIT:
            pszRecordType = "update";
            break;

        default:
            pszRecordType = "unknown";
            break;
    }

    return pszRecordType;
}
