#ifndef _SSLCONNECTION_H_
#define _SSLCONNECTION_H_

#pragma once

#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <string>

#ifdef _WIN32

#include <WinSock2.h>
#include <WS2tcpip.h>

#define close closesocket
#define SHUT_RDWR SD_BOTH

#else

#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <unistd.h>

#define SOCKET         int
#define INVALID_SOCKET -1

#endif

#include "openssl/ssl.h"
#include "openssl/err.h"

class CSslConnection
{
public:
    ~CSslConnection();

    SSL *getSsl();
    bool RecvBytes(char *pb, uint32_t cb);

    static CSslConnection *Create(SOCKET socket);
    static void Initialize(const std::string &certificateFilename, const std::string &privateKeyFilename);
    static void CleanUp();

private:
    CSslConnection();
    bool InitializeContext(SOCKET socket);

    char *ossl_err_as_string();
    bool LoadSSLCertificates();
    void Shutdown();

    static std::string s_certificateFilename;
    static std::string s_privateKeyFilename;

    SSL *m_ssl;
    SOCKET m_socket;
    SSL_CTX *m_pCtx;
};

#endif  // _SSLCONNECTION_H_

