#ifndef _BLUEMACDECODER_H_
#define _BLUEMACDECODER_H_

#pragma once

#include <stdint.h>

#include <string>

#define BLUEMAC_REDUCTIONTYPE_UNKNOWN   -1
#define BLUEMAC_REDUCTIONTYPE_OFF        0
#define BLUEMAC_REDUCTIONTYPE_NEWHIT     1
#define BLUEMAC_REDUCTIONTYPE_UPDATEDHIT 2

typedef struct _BLUEMACSAMPLE
{
    uint32_t timestamp;
    int8_t rssi;
    uint16_t manufacturer;
    uint32_t oui;
    uint32_t reductionType;
    uint32_t reductionHitCount;
    uint32_t reductionDuration;
    char objectId[41];
    char radioType[5];
} BLUEMACSAMPLE, *PBLUEMACSAMPLE;

bool BluemacDecodeRawSample(
    const char *pchSample,
    PBLUEMACSAMPLE pBluemacSample
    );

std::string BluemacGetRecordType(uint32_t recordType);

#endif  // _BLUEMACDECODER_H_

