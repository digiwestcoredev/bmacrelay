#ifndef _CLIENT_H_
#define _CLIENT_H_

#pragma once

bool relay_server_start(
    uint32_t port,
    const std::string &allowedIPAddress,
    const std::string &certificateFile,
    const std::string &privateKeyFile
    );

void relay_server_stop();

#endif  // _CLIENT_H_

