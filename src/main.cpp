// BluemacDataServer.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <stdio.h>
#include <string.h>

#include <string>

#ifdef _WIN32

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#else

#include <signal.h>

#endif

#include "relayserver.h"

#ifdef _WIN32

unsigned int terminateIncomingConnectionsLoop = 0;

BOOL WINAPI sigHandler(
    DWORD fdwCtrlType
    )
{
    switch (fdwCtrlType)
    {
        // Handle the CTRL-C signal.
        case CTRL_C_EVENT:
        case CTRL_CLOSE_EVENT:
            relay_server_stop();

            return TRUE;

        default:
            return FALSE;
    }
}

#else

//
// SIGTERM handler & CTRL-C/SIGINT handler
//
volatile sig_atomic_t terminateIncomingConnectionsLoop = 0;

void sigHandler(
    int sigNumber
    )
{
    (void) sigNumber;

    stop_relay_server();
}

#endif

void SetSignalHandler()
{
#ifdef _WIN32
    SetConsoleCtrlHandler((PHANDLER_ROUTINE) sigHandler, TRUE);
#else
    // define sigterm/sigint handler (to gracefully exit from kill & CTRL-C)
    struct sigaction action;

    memset(&action, 0, sizeof(struct sigaction));

    action.sa_handler = sigHandler;

    sigaction(SIGTERM, &action, NULL);
    sigaction(SIGINT, &action, NULL);
#endif
}

int main(
    int argc,
    char *argv[]
    )
{
    //
    // Usage
    //     -p port
    //     -f certificate file
    //     -k private key file
    //     -a allowed IP address (for incoming connections)
    //
    bool bUsage = false;
    int port = 0;
    std::string allowedIPAddress;
    std::string certificateFile;
    std::string privateKeyFile;

    // Every switch requires a value, so ensure there are an even number of arguments (not including arg 0, which is the program name)
    if ((argc - 1) % 2 != 0)
    {
        bUsage = true;
    }
    else
    {
        for (int i = 1; i < argc; i++)
        {
            const char *pArg = (const char *) argv[i++];
            const char *pValue = (const char *) argv[i];

            if (!strcmp(pArg, "-p"))
            {
                port = atoi(pValue);
            }
            else if (!strcmp(pArg, "-f"))
            {
                certificateFile = pValue;
            }
            else if (!strcmp(pArg, "-k"))
            {
                privateKeyFile = pValue;
            }
            else if (!strcmp(pArg, "-a"))
            {
                allowedIPAddress = pValue;
            }
            else
            {
                // Unrecognized option
                bUsage = true;
                break;
            }
        }
    }

    if ((port == 0) || certificateFile.empty() || privateKeyFile.empty())
    {
        bUsage = true;
    }

    if (bUsage)
    {
        printf("Usage: %s -p port -f certificateFile -k privateKeyFile [-a allowedIPAddress]\n", argv[0]);

        return 1;
    }

    SetSignalHandler();

    relay_server_start(port, allowedIPAddress, certificateFile, privateKeyFile);

    return 0;
}

